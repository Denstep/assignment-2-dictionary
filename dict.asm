%include "lib.inc"

global find_word

find_word:
.loop:

	test rsi, 0
	je .error
	
	push rdi
	push rsi

	add rsi, 8
	call string_equals

	pop rsi
	pop rdi

	cmp rax, 1
	je .end

	mov rsi, [rsi]
	jmp .loop

.end:
	mov rax, rsi
	ret

.error:
	xor rax, rax
	ret