%include "words.inc"
%include "lib.inc"
%define buffer_size 255
%define STDERR 2

section .bss
buffer:
	resb buffer_size

section .data
    key_not_found: db "Value not found in the list", 0
    buf_over: db "Key-word does not fit in the buffer", 0
    
section .text
extern find_word
global _start

read_string:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8

    .loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi

    test rax, rax
    jz .end

    cmp rax, 0xA
    je .end

    mov byte[rdi+r8], al
    inc r8
    cmp r8, rsi
    jg .overflow_end
    jmp .loop

    .overflow_end:
    xor rax, 0
    ret

    .end:
    mov byte[rdi+r8], 0x0
    mov rdx, r8
    mov rax, rdi
    ret


_start:
	mov rdi, buffer
	mov rsi, buffer_size 
	call read_string
	test rax, rax
	je .buffer_error

	mov rdi, buffer
	mov rsi, next
	call find_word

	test rax, rax
	je .not_key_error

	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	mov rdi, 0
	call exit

	.buffer_error:
	mov rdi, buf_over
	jmp .error_end

	.not_key_error:
	mov rdi, key_not_found
	jmp .error_end
	
	.error_end:
	call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, STDERR
    syscall
    call exit




