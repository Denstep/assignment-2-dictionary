ASM=nasm
ASMFLAGS=-f elf64
LD=ld
RM=rm

program: main.o dict.o lib.o
    $(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm dict.asm words.inc colon.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean
clean:
    $(RM) *.o program	
